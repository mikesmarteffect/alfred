const { expect } = require('chai')
const ucFirst = require('../src/helpers/ucFirst')
const getRandomIndex = require('../src/helpers/getRandomIndex')
const breezeRequest = require('../src/helpers/breezeRequest')
const getHoursFromMinutes = require('../src/helpers/getHoursFromMinutes')

describe('HELPERS', () => {
    describe('ucFirst()', () => {
        it('returns a capatalised word', () => {
            const actual = ucFirst('foo')
            const expected = 'Foo'
            return expect(actual).to.equal(expected)
        })
    })

    describe('getRandomIndex()', () => {
        it('return a random index from an array', () => {
            let okay

            for (let i = 0; i < 100; i++) {
                const actual = getRandomIndex(['foo', 'bar', 'baz'])

                if (actual) okay = actual
                else {
                    okay = false
                    break
                }
            }

            expect(okay).to.be.ok
        })
    })

    describe('breezeRequest()', () => {
        it('GET response from the breeze API', () => {
            breezeRequest('projects.json')
            .then(res => expect(res).to.be.ok)
        })

        it('POST response from the breeze API', () => {
            const data = { report_type: 'timetracking', start_date: 'today'}

            breezeRequest('reports.json', { method: 'post', data })
            .then(res => expect(res).to.be.ok)
        })
    })

    describe('getHoursFromMinutes', () => {
        it('returns hours and minutes', () => {
            const actual = getHoursFromMinutes(125)
            const expected = '2h5m'

            expect(actual).to.equal(expected)
        })

        it('returns just minutes', () => {
            const actual = getHoursFromMinutes(50)
            const expected = '50m'

            expect(actual).to.equal(expected)
        })

        it('returns a whole hour', () => {
            const actual = getHoursFromMinutes(60)
            const expected = '1h'

            expect(actual).to.equal(expected)
        })
    })
})
