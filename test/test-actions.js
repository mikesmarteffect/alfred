const { expect } = require('chai')
const whatDoYouDo = require('../src/actions/whatDoYouDo')
const sayHello = require('../src/actions/sayHello')
const getFilmInfo = require('../src/actions/getFilmInfo')
const getProjectList = require('../src/actions/getProjectList')
const getProjectsByDay = require('../src/actions/getProjectsByDay')

describe('ACTIONS', () => {
    describe('whatDoYouDo()', () => {
        it('returns a response', () => {
            const actual = whatDoYouDo()
            return expect(actual).to.be.ok
        })
    })

    describe('sayHello()', () => {
        it('returns a response', () => {
            const actual = sayHello({ test: ['', 'hello', 'foo'] })
            return expect(actual).to.be.ok
        })
    })

    describe('getFilmInfo()', () => {
        it('returns recognised film release date', () => {
            return getFilmInfo({ test: ['', 'goodfellas'] }, 'release_date')
            .then(res => expect(res).to.be.ok)
        })

        it('rejects an unrecognised film', () => {
            return getFilmInfo({ test: ['', 'asfsdf'] }, 'release_date')
            .then().catch(err => expect(err).to.be.ok)
        })
    })

    describe('getFilmYear()', () => {
        it('returns recognised film plot', () => {
            return getFilmInfo({ test: ['', 'goodfellas'] }, 'plot')
            .then(res => expect(res).to.be.ok)
        })

        it('rejects an unrecognised film', () => {
            return getFilmInfo({ test: ['', 'asfsdf'] }, 'plot')
            .then().catch(err => expect(err).to.be.ok)
        })
    })

    describe('getProjectList()', () => {
        it('returns a string of Breeze projects', () => {
            return getProjectList().then(res => expect(res).to.be.ok)
        })
    })

    describe('getProjectsByDay()', () => {
        it('returns a list of today\'s projects', () => {
            return getProjectsByDay().then(res => expect(res).to.be.ok)
        })
    })
})
