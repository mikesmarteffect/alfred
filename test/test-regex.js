const { expect } = require('chai')
const {
    what_do_you_do,
    say_hello_to,
    when_was_released,
    what_is_about,
    project_list,
    project_day
} = require('../src/language')

describe('REGEX', () => {
    describe('What do you do?', () => {
        const whatDoYouDo = 'what do you do'
        const whatCanYouDo = 'what can you do'
        const whoAreYou = 'who are you'

        it(whatDoYouDo, () => {
            const test = whatDoYouDo.match(what_do_you_do)
            return expect(test).to.be.ok
        })

        it(whatCanYouDo, () => {
            const test = whatCanYouDo.match(what_do_you_do)
            return expect(test).to.be.ok
        })

        it(whoAreYou, () => {
            const test = whoAreYou.match(what_do_you_do)
            return expect(test).to.be.ok
        })
    })

    describe('Say hello to ...', () => {
        const sayHelloToFoo = 'say hello to foo'
        const sayHiToFoo = 'say hi to foo'

        it(sayHelloToFoo, () => {
            const test = say_hello_to.exec(sayHelloToFoo)
            const actual = `${test[1]} ${test[2]}`
            const expected = 'hello foo'
            return expect(actual).to.equal(expected)
        })

        it(sayHiToFoo, () => {
            const test = say_hello_to.exec(sayHiToFoo)
            const actual = `${test[1]} ${test[2]}`
            const expected = 'hi foo'
            return expect(actual).to.equal(expected)
        })
    })

    describe('when was ... released', () => {
        const whenWasReleased = 'when was foo released'
        const whenDidComeOut = 'when did foo come out'

        it(whenWasReleased, () => {
            const test = when_was_released.exec(whenWasReleased)
            const actual = test[1]
            const expected = 'foo'
            return expect(actual).to.equal(expected)
        })

        it(whenDidComeOut, () => {
            const test = when_was_released.exec(whenDidComeOut)
            const actual = test[1]
            const expected = 'foo'
            return expect(actual).to.equal(expected)
        })
    })

    describe('What is ... about', () => {
        const whatWas = 'what was foo about'
        const whatIs = 'what is foo about'

        it(whatWas, () => {
            const test = what_is_about.exec(whatWas)
            const actual = test[1]
            const expected = 'foo'
            return expect(actual).to.equal(expected)
        })

        it(whatIs, () => {
            const test = what_is_about.exec(whatIs)
            const actual = test[1]
            const expected = 'foo'
            return expect(actual).to.equal(expected)
        })
    })

    describe('List breeze projects', () => {
        const showMeAll = 'Show me all projects'
        const showMeAList = 'show me a list of projects'
        const showMeAllThe = 'show me all the projects'
        const listAll = 'list all projects'
        const list = 'list projects'

        it(showMeAll, () => {
            const test = showMeAll.match(project_list)
            return expect(test).to.be.ok
        })

        it(showMeAList, () => {
            const test = showMeAList.match(project_list)
            return expect(test).to.be.ok
        })

        it(showMeAllThe, () => {
            const test = showMeAllThe.match(project_list)
            return expect(test).to.be.ok
        })

        it(listAll, () => {
            const test = listAll.match(project_list)
            return expect(test).to.be.ok
        })

        it(list, () => {
            const test = list.match(project_list)
            return expect(test).to.be.ok
        })
    })

    describe('Get breeze project by day', () => {
        const showMeTodays = 'Show me todays projects'
        const showMeToday_s = 'Show me today\'s projects'
        const showMeProjectsToday = 'Show me projects worked on today'
        const whatArePeopleWorking = 'What are people working on'
        const whatHavePeopleWorked = 'What have people worked on'
        const whatHavePeopleBeen = 'What have people been working on'

        it(showMeTodays, () => {
            const test = showMeTodays.match(project_day)
            return expect(test).to.be.ok
        })

        it(showMeToday_s, () => {
            const test = showMeToday_s.match(project_day)
            return expect(test).to.be.ok
        })

        it(showMeProjectsToday, () => {
            const test = showMeProjectsToday.match(project_day)
            return expect(test).to.be.ok
        })

        it(whatArePeopleWorking, () => {
            const test = whatArePeopleWorking.match(project_day)
            return expect(test).to.be.ok
        })

        it(whatHavePeopleWorked, () => {
            const test = whatHavePeopleWorked.match(project_day)
            return expect(test).to.be.ok
        })

        it(whatHavePeopleBeen, () => {
            const test = whatHavePeopleBeen.match(project_day)
            return expect(test).to.be.ok
        })
    })
})
