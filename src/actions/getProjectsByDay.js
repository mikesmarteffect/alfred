const breezeRequest = require('../helpers/breezeRequest')
const printProjectsList = require('../helpers/printProjectsList')
const getHoursFromMinutes = require('../helpers/getHoursFromMinutes.js')
const AlfredsMemory = require('../memory')

const projects = {}
const projectCache = []

function buildProjectList (activities) {
    activities.forEach(activity => {
        const { project_id, tracked, user_id } = activity

        if (projectCache.indexOf(project_id) < 0) {
            projects[project_id] = activity
            projects[project_id]['users'] = [user_id]
        }
        else {
            projects[project_id].tracked += tracked
            projects[project_id].users.push(user_id)
        }

        projectCache.push(project_id)
    })
}

function printActivityReport () {
    let response = ''

    Object.keys(projects).forEach(key => {
        const { users, name, tracked } = projects[key]
        const userNames = []
        const printedUsers = ''

        users.forEach(user => {
            AlfredsMemory.get(user, (err, name) => {
                if (err) console.log(`ERROR GETTING PROJECT CACHE: ${err}`)
                if (name) {
                    const firstname = `${name.split(' ')[0]}`

                    if (userNames.indexOf(firstname) < 0) {
                        userNames.push(firstname)
                    }
                }
            })
        })

        if (tracked) {
            response += `> *${name}*\n> ${userNames.join(', ')} -  ${getHoursFromMinutes(tracked)}\n\n`
        }
    })

    return response
}

function projectRequest (query, page, data, reject, callback) {
    let endpoint = page ? `${query}?page=${page}` : query

    breezeRequest(endpoint, { method: 'post', data}).then(res => {
        buildProjectList(res)

        if (res.length === 100) {
            page++
            projectRequest(query, page, data, callback)
        }
        else {
            callback()
        }
    }).catch(err => reject(err))
}

module.exports = (command) => {
    // const day = command.test[1]
    const query = 'reports.json'
    let page = 0
    let data = {
        report_type: 'timetracking',
        start_date: 'today'
    }

    return new Promise ((resolve, reject) => {
        projectRequest(query, page, data, reject, () => {
            Object.keys(projects).forEach(key => {
                const project = projects[key]

                AlfredsMemory.get(project.project_id, (err, name) => {
                    if (err) console.log(`ERROR GETTING PROJECT CACHE: ${err}`)
                    project['name'] = name
                })
            })

            resolve(printActivityReport())
        })
    })
}
