const request = require('request')

module.exports = function (command, mode) {
    const userFilm = command.test[1].trim()
    const sanitizedFilm = userFilm.trim().replace(' ', '+')
    return new Promise((resolve, reject) => {
        request(`http://www.omdbapi.com/?t=${sanitizedFilm}&y=short&r=json`, (err, res, body) => {
            if (err) reject('hmmmm I\'m having a hard time answering that')

            const film = JSON.parse(body)

            if (film.Response !== 'False') {
                switch (mode) {
                case 'plot':
                    resolve(`>${film.Plot}`)
                    break
                case 'release_date':
                    resolve(`_${film.Title}_ came out on the *${film.Released}*`)
                    break
                }
            }

            else {
                reject(`I\'ve never heard of the film _${userFilm}_`)
            }
        })
    })
}
