const breezeRequest = require('../helpers/breezeRequest')
const colors = require('colors')
const printProjectsList = require('../helpers/printProjectsList')

/**
 * returns string of breeze projects
 * @return {string}
 */
module.exports = (config = { mode: 'string' }) => {
    return new Promise((resolve, reject) => {
        breezeRequest('projects.json')
        .then(res => {
            if (config.mode === 'string') resolve(printProjectsList(res))
            else if (config.mode === 'raw') resolve(res)
        })
        .catch((err) => {
            console.log(`ERROR: ${err}`.red)
            return reject('I\'m having trouble getting a response from Breeze')
        })
    })
}
