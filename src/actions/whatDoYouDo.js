/**
 * What Do You Do?
 * @return {string}
 */
module.exports = function () {
    const skills = [
        '> *Information on Breeze projects*\n> _"Alfred list all projects"_, _"Alfred what have people been working on?"_\n',
        '> *Movie trivia*\n> _"Alfred when did Goodfellas come out?"_, _"Alfred what is The Human Centipede about?"_\n'
    ]

    const list = skills.map(skill => {
        return `\n${skill}`
    })

    return ':wave: *Hello*, I\'m Alfred. Try asking me some questions, I\'m trained to deal with the following things...' + list.join('')
}
