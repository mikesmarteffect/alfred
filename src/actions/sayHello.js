const ucFirst = require('../helpers/ucFirst')

/**
 * Say Hello To ...
 * @param  {Object} command
 * @return {String}
 */
module.exports = function (command) {
    return `:wave: ${ucFirst(command.test[1])} ${ucFirst(command.test[2])}`
}
