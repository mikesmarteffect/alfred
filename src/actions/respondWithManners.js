const getRandomIndex = require('../helpers/getRandomIndex')
const manners = [
    'No problemo!',
    'You\'re welcome',
    'Any time!',
    'No worries :+1:',
    'It was my pleasure',
    ':ok_hand:',
    ':+1:'
]

module.exports = () => {
    return getRandomIndex(manners)
}
