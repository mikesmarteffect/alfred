module.exports = {
    what_do_you_do: /(who|what)\s(can|are|do)\s(you)(?:\sdo|)/i,
    say_hello_to: /say\s(hello|hi)\sto\s(\w+)/i,
    when_was_released: /(?:when)\s(?:was|did)\s(.*?)\s(?:release|come\sout)/i,
    what_is_about: /what\s(?:is|was|)\s(.*?)\sabout/i,
    project_list: /(?:show\sme\s(the|all|a)\s(?:list\sof\sprojects|(?:the\sprojects|projects))|list\sprojects|list\sall\sprojects)/i,
    project_day: /(?:show\sme\s(?:(.*)(?:(?:'|)s)\sprojects|projects\sworked\son\s(today))|what\s(?:are|have)\speople\s(?:working|worked|been\sworking)\son(?:\stoday|))/i,
    thank_you: /(?:thanks|cheers|ta|thankyou|thank)/i
}
