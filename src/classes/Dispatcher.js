const {
    what_do_you_do,
    say_hello_to,
    when_was_released,
    what_is_about,
    project_list,
    project_day,
    thank_you
} = require('../language')

const colors = require('colors')
const getRandomIndex = require('../helpers/getRandomIndex')
const whatDoYouDo = require('../actions/whatDoYouDo')
const sayHello = require('../actions/sayHello')
const getFilmInfo = require('../actions/getFilmInfo')
const getProjectList = require('../actions/getProjectList')
const getProjectsByDay = require('../actions/getProjectsByDay')
const respondWithManners = require('../actions/respondWithManners')

class Dispatcher {
    constructor (config) {
        this.bot = config.BOT_NAME
        this.message = config.text
        this.context = config.context
        this.commands = [
            { test: what_do_you_do, key: 'what_do_you_do' },
            { test: say_hello_to, key: 'say_hello_to' },
            { test: when_was_released, key: 'when_was_released' },
            { test: what_is_about, key: 'what_is_about' },
            { test: project_list, key: 'project_list' },
            { test: project_day, key: 'project_day' },
            { test: thank_you, key: 'thank_you' }
        ]
        this.commandLength = Object.keys(this.commands).length
        this.singleResponse = ['That\'s me! What can I do to help?', 'Yes?', 'At your service, how can I help?']
        this.processResponse()
    }

    processResponse () {
        const name = new RegExp(this.bot, 'i')
        const messageLength = this.message.split(' ').length

        this.nameWasMentioned = this.message.match(name)

        // Check if bot is being refrenced by name
        if (this.nameWasMentioned !== null && messageLength > 1) {
            console.log('\nCommand response'.yellow)
            this.processedCommand = this.test()
        }
        else if (this.nameWasMentioned) {
            console.log('\nName response'.yellow)
            let shouting = false
            if (this.message.match(/ALFRED/)) shouting = true
            this.processedCommand = { key: 'alfred', shouting }
        }
        else if (this.context) {
            console.log('\nContext response'.yellow)
            this.processedCommand = this.test()
        }
    }

    /**
     * Test input against command regex library
     */
    test () {
        for (let i = 0; i < this.commandLength; i++) {
            const { test, key } = this.commands[i]
            const re = test.exec(this.message)
            if (re) return { test: re, key: key }
        }
    }

    /**
     * Return responses
     */
    respond () {
        console.log(`processed command: ${this.processedCommand}\n`.yellow)

        return new Promise((respond, reject) => {
            const { key } = this.processedCommand || { key: null }
            const { shouting } = this.processedCommand || { shouting: null }
            let response

            switch (key) {
            case 'alfred':
                response = getRandomIndex(this.singleResponse)
                respond(shouting ? response.toUpperCase() : response)
                break
            case 'what_do_you_do':
                respond(whatDoYouDo())
                break
            case 'say_hello_to':
                respond(sayHello(this.processedCommand))
                break
            case 'project_list':
                getProjectList()
                .then(res => respond(res))
                .catch(err => reject(err))
                break
            case 'project_day':
                getProjectsByDay(this.processedCommand)
                .then(res => respond(res))
                .catch(err => reject(err))
                break
            case 'when_was_released':
                getFilmInfo(this.processedCommand, 'release_date')
                .then(res => respond(res))
                .catch(err => reject(err))
                break
            case 'what_is_about':
                getFilmInfo(this.processedCommand, 'plot')
                .then(res => respond(res))
                .catch(err => reject(err))
                break
            case 'thank_you':
                respond(respondWithManners())
                break
            default:
                reject({ mentioned: this.nameWasMentioned })
                break
            }
        })
    }
}

module.exports = Dispatcher
