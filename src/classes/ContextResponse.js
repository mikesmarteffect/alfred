const Dispatcher = require('./Dispatcher')
const {
    thank_you
} = require('../language')

class ContextResponse {
    constructor (config) {
        this.bot = config.BOT_NAME
        this.message = config.text
        this.commands = [
            { test: thank_you, key: 'thank_you' }
        ]

        this.processResponse()

        console.log(this.processedCommand)
    }

    respond () {
        console.log('COMMAND: ', this.processedCommand)

        return new Promise((respond, reject) => {
            const { key } = this.processedCommand || { key: null }
            const { shouting } = this.processedCommand || { shouting: null }
            let response

            console.log(key)
        })
    }
}

module.exports = ContextResponse
