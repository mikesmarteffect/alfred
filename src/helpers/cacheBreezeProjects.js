const colors = require('colors')
const getProjectList = require('../actions/getProjectList')
const AlfredsMemory = require('../memory')

module.exports = () => {
    getProjectList({ mode: 'raw' })
    .then(res => {
        res.forEach(project => {
            AlfredsMemory.set(project.id, project.name, err => {
                if (err) console.log(`ERROR CACHING PROJECTS" ${err}`.red)
            })
        })

        console.log('Breeze projects cached'.yellow)
    })
    .catch(err => console.log(`ERROR GETTING PROJECTS: ${err}`.red))
}
