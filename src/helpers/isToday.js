/**
 * Check if a date is today
 * @param  {number} date date to check
 * @return {boolean}
 */
module.exports = function (date) {
    const today = new Date()
    return date.setHours(0, 0, 0, 0) === today.setHours(0, 0, 0, 0)
}
