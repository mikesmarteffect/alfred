module.exports = (message, name) => {
    const re = new RegExp(name, 'i')
    return message.match(re)
}
