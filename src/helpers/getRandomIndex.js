/**
 * Return random item from array
 * @param  {Array} array
 */
module.exports = function (array) {
    return array[Math.floor(Math.random() * array.length)]
}
