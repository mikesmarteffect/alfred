const colors = require('colors')
const breezeRequest = require('./breezeRequest')
const AlfredsMemory = require('../memory')

module.exports = () => {
    breezeRequest('users.json')
    .then(res => {
        res.forEach(user => {
            AlfredsMemory.set(user.id, user.name, err => {
                if (err) console.log(`ERROR CACHING PROJECTS" ${err}`.red)
            })
        })

        console.log('Breeze users cached'.yellow)
    })
    .catch(err => console.log(`ERROR GETTING PROJECTS: ${err}`.red))
}
