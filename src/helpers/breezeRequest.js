const request = require('request')
const { BREEZE_TOKEN } = require('dotenv').config()
const { BREEZE_API } = require('../config')

const defaults = { method: 'get', data: false }

module.exports = (query, options) => {
    const url = BREEZE_API + query
    const { data, method } = Object.assign(defaults, options)

    console.log('endpoint: ', url, ' | method: ', method.toUpperCase())

    if (query !== 'reports.json' && method === 'post') {
        return console.log(`TRYING TO SEND A POST REQUEST TO ${query}`.red)
    }

    return new Promise((resolve, reject) => {
        if (BREEZE_TOKEN) {
            request({
                method,
                url,
                form: data,
                auth: { user: BREEZE_TOKEN }
            }, (err, res, body) => {
                if (err || res.statusCode !== 200) {
                    console.log('ERROR: ', err)
                    console.log('REQUEST: ', url)
                    console.log('RES: ', res.statusCode)
                    return reject('I\'m having trouble getting a response from Breeze')
                }

                resolve(JSON.parse(body))
            })
        }
        else {
            reject('No Breeze API token provided')
        }
    })
}
