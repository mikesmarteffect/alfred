/**
 * Determine if a message is related to the last
 * @param  {Object} context
 * @param  {Object} msg
 * @return {Boolean}
 */
module.exports = (context, msg) => {
    const { user, channel } = msg
    return context.user === user && context.channel === channel
}
