/**
 * Builds project list string
 * @param  {object} projects
 * @return {string}
 */
module.exports = (projects) => {
    let projectHTML = ''

    projects.forEach(project => {
        if (!project.archived && project.name) {
            const split = project.name.split(' - ')
            let projectName

            if (split.length > 1) projectName = `*${split[0]}* - ${split[1]}`
            else projectName = `*${split[0]}*`

            projectHTML += `> ${projectName}\n`
        }
    })

    return projectHTML
}
