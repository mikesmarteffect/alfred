module.exports = (minutes) => {
    const minInHour = 60
    const howManyhours = Math.floor(minutes / minInHour)
    const hourSymbol = 'h'
    const minuteSymbol = 'm'
    let mins = false
    let hours = false

    if (howManyhours < 1) mins = minutes
    else {
        hours = howManyhours
        mins = minutes % minInHour
    }

    return `${hours ? hours + hourSymbol : ''}${mins ? mins + minuteSymbol : ''}`
}
