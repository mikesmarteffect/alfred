const { RtmClient, CLIENT_EVENTS, RTM_EVENTS, WebClient } = require('@slack/client')
const { BOT_TOKEN, BREEZE_TOKEN } = require('dotenv').config()
const { BOT_NAME } = require('./src/config')
const colors = require('colors')
const cacheBreezeProjects = require('./src/helpers/cacheBreezeProjects')
const cacheBreezeUsers = require('./src/helpers/cacheBreezeUsers')
const Dispatcher = require('./src/classes/Dispatcher')
const wasAlfredMentioned = require('./src/helpers/wasAlfredMentioned')
const isContextual = require('./src/helpers/isContextual')

/**
 * Alfreds Cerebral Cortex
 */

// cache project names against IDs
cacheBreezeProjects()
// cache user names against IDs
cacheBreezeUsers()

/**
 * Quit process if env vars do not exists
 */

if (!BOT_TOKEN || !BREEZE_TOKEN) {
    if (!BOT_TOKEN) console.log('NO BOT TOKEN PROVIDED'.red)
    if (!BREEZE_TOKEN) console.log('NO BREEZE API TOKEN PROVIDED'.red)
    process.exit(1)
}

/**
 * Create Alfred and his global language vars
 */

const alfred = new RtmClient(BOT_TOKEN)
const errorMessage = 'I\'m sorry I didn\'t understand that'

/**
 * Getting info from the Slack wep API
 */

const web = new WebClient(BOT_TOKEN)

// Get Alfred's slack ID
web.users.list((err, users) => {
    if (err) console.log(`ERROR GETTING USERS FROM WEB API: ${err}`.red)
    users.members.forEach(user => {
        if (user.name === 'alfred' && user.is_bot) {
            alfred['slack_id'] = user.id
        }
    })
})

/**
 * Connecttion openned event
 */

alfred.on(CLIENT_EVENTS.RTM.RTM_CONNECTION_OPENED, () => {
    console.log('Alfred is awake!')
})

/**
 * Handle messages
 */

let context = { user: false, channel: false, msg: false }

alfred.on(RTM_EVENTS.MESSAGE, (msg) => {
    const { channel, text, user } = msg

    if (wasAlfredMentioned(text, BOT_NAME)) {
        const Dispatch = new Dispatcher({ text, BOT_NAME })

        // make sure alfred doesn't talk to himself
        if (user !== alfred.slack_id) {
            alfred.sendTyping(channel)

            Dispatch.respond()
            .then(res => {
                alfred.sendMessage(res, channel)
            })
            .catch((err) => {
                console.log(`REPOSNSE ERROR: ${JSON.stringify(err)}`.red)
                if (err && !err.nameWasMentioned) return
                else alfred.sendMessage(errorMessage, channel)
            })
        }
    }
    else if (isContextual(context, msg)) {
        const Response = new Dispatcher({ text, BOT_NAME, context: true })

        Response.respond()
        .then(res => {
            alfred.sendMessage(res, channel)
        })
        .catch((err) => {
            console.log(`CONTEXT ERROR: ${JSON.stringify(err)}`.red)
        })
    }

    context.user = user
    context.channel = channel
    context.msg = msg
})

/**
 * HE'S ALIVE!!!!!
 */

alfred.start()
